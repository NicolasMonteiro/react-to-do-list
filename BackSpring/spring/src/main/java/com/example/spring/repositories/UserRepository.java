package com.example.spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.spring.models.UserModel;

import java.util.UUID;


@Repository
public interface UserRepository extends JpaRepository<UserModel, UUID> {

}
