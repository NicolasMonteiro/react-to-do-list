import './App.css';

import { Outlet } from 'react-router-dom';
import TopBar from './Components/TopBar/TopBar';
import SideBar from './Components/SideBar/SideBar';
import React, {useState} from 'react';

function App(){
    const [isSideBarVisible, setIsSideBarVisible] = useState(false);

    const toggleSidebar = () => {
        setIsSideBarVisible(!isSideBarVisible);
    };

    return(
        <div className="App">
            <section className="section-top-App"><TopBar onSidebarToggle={toggleSidebar} /></section>
            <section className={`section-side-App ${isSideBarVisible ? 'open' : ''}`}><SideBar /></section>
            <section className="section-outlet-App"><Outlet /></section>
            
            
           
            
        </div>
        

    )
};

export default App;
