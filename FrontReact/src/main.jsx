import React, { Children } from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import {createBrowserRouter, RouterProvider} from "react-router-dom";

import App from './App';
import PageHome from './Components/PageHome/PageHome'
import Register from './Components/PageRegister/Register'
import Login from './Components/PageLogin/Login'
/*
const router = createBrowserRouter([
  {
    path: "/",
    element: <PageHome />
  },
  {
    path: "register",
    element: <Register />
  },  
  {
    path: "login",
    element: <Login />
  },
]); */

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: [
      {
        path: "/",
        element: <PageHome />
      },
      {
        path: "register",
        element: <Register />
      },  
      {
        path: "login",
        element: <Login />
      },
    ]
  },
]);


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
