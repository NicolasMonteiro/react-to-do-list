import {React, useState} from 'react';
import ButtonSubmit from '../ButtonSubmit/ButtonSubmit'

import './Register.scss';

const Register = () => {

    const [name, setName] = useState("");
    const [user, setUser] = useState("");
    const [email, setEmail] = useState("");
    const [passWord, setPassWord] = useState("");
    const [repeatPass, setRepeatPass] = useState("");

    const [envio, setEnvio] = useState(false);
    const [error, setError] = useState("");

    const handleSubmit = (event) => {
        event.preventDefault();
        console.log("Tentativa envio");

        if(name && user && email && passWord && repeatPass){
            if (passWord !== repeatPass) {
                setError("As senhas não coincidem");
                return;
            }
            setError("");

            setEnvio(true);
            console.log("envio completo");
            console.log("dados: " + name, user, email, passWord, repeatPass);
            
        }

    }

    return (

        <section className="reg-section-main">
            <form onSubmit={handleSubmit}>

                <div className="reg-div-tittle"><h1> Register Page</h1></div>
                
                    
                <div className="reg-div-name">
                    <p>Full name:</p>
                    <input 
                        type="text" 
                        name="name" 
                        id="name" 
                        placeholder="Enter your full name..." 
                        value={name} 
                        onChange={(e) => setName(e.target.value)}
                        required
                    />
                </div>

                <div className="reg-div-user">
                    <p>Username:</p>
                    <input 
                        type="text" 
                        name="user" 
                        id="user" 
                        placeholder="Choose a username..." 
                        value={user} 
                        onChange={(e) => setUser(e.target.value)}
                        required
                    />
                </div>

                <div className="reg-div-email">
                    <p>Email:</p>
                    <input 
                        type="email" 
                        name="email" 
                        id="email" 
                        placeholder="Enter your email address..." 
                        value={email} 
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                </div>

                <div className="reg-div-password">
                    <p>Enter a password:</p>
                    <input 
                        type="text" 
                        name="password" 
                        id="password" 
                        placeholder="Create a password..." 
                        value={passWord} 
                        onChange={(e) => setPassWord(e.target.value)}
                        required
                    />
                </div>

                <div className="reg-div-password">
                    <p>Repeat your password:</p>
                    <input 
                        type="text" 
                        name="passwordRepeat" 
                        id="passwordRepeat" 
                        placeholder="Repeat your password..." 
                        value={repeatPass} 
                        onChange={(e) => setRepeatPass(e.target.value)}
                        required
                    />

                    {error && <p style={{ color: 'red' }}>{error}</p>}  
                </div>


                
                <ButtonSubmit />

            </form>

            <div> {envio && <h1>Seja bem vindo, {name}!</h1>} </div>

        </section>      
    );
};

export default Register;