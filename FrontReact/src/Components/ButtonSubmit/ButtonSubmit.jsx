import React from 'react'

import './ButtonSubmit.scss'


const ButtonSubmit = () => {
  return (
    <div className="bs-div-button">
        <button type="submit" className='bs-button-submit'>Enviar</button>

    </div>
  )
}

export default ButtonSubmit;