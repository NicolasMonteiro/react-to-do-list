import React from 'react'
import './PageHome.scss'

const PageHome = () => {
  return (
    <div>
      <section className="home-section-main">
        <div className="home-div-tittle">
          <h1>Bem Vindo!</h1>
        </div>
        <div className="home-div-description">
          <p> 
            O site como função principal o gerenciamento de tarefas e hábitos 
            periódicos, ajudando o usuário com sua organização diária.
          </p>

          <h1>Cadastre um Hábito ainda hoje e comece a reogranize sua vida</h1>
        </div>
      </section>
    </div>
  )
}

export default PageHome
