import React from 'react'
import './TopBar.scss';

const TopBar = ({ onSidebarToggle  }) => {

    const handleClick = () => {
        onSidebarToggle();

    }

  return (
    <section className = "top-section-main"  >
        <div className="top-div-menu" onClick={handleClick}>
            <button className="top-button-menu">
                <img src="/icon-menu.png" alt="menu-icon" width="38" height="36"/>
            </button>

        </div>

        <div className="top-div-centro">
            <div className = "top-div-logo">   
                <a href = "/">
                    <h1>Task List</h1>
                </a> 
            </div>
        </div> 

        <div className="top-div-search">
            <button className="button-search">
                <img src="/icon-search.png" alt="search-icon" width="38" height="38"/>
            </button>

        </div>

    </section>
  )
}

export default TopBar
