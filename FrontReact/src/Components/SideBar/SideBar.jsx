import React from 'react'
import './SideBar.scss'

const SideBar = () => {
  return (
    <section className="side-section-main">
        <a href="/">
            <div className="div-link">
                <h1>Home</h1>
            </div>
        </a>
        <hr />

        <a href="/to-do-List">
            <div className="div-link">
                <h1>To-do List</h1>
            </div>
        </a>
        <hr />

        <a href="/register">
            <div className="div-link">
                <h1>Register (teste)</h1>
            </div>
        </a>
        <hr />

        <a href="/login">
            <div className="div-link">
                <h1>Login (teste)</h1>
            </div>
        </a>
        <hr />

        <a href="/acount">
            <div className="div-link">
                <h1>Acount</h1>
            </div>
        </a>
        <hr />

        <a href="/about">
            <div className="div-link">
                <h1>About</h1>
            </div>
        </a>
        <hr />


    </section>
  )
}

export default SideBar;
